--- Creado el modulo Categoria ---

-- Persistencia --

- Se agregan dos nuevas tablas: 
  . Category, con las columnas usuarrio y nombre de categoria
  . Category_task resultado de la relacion M - M entre tarea y categoria, lo que implica que una tarea puede estar en muchas categorias y una categoria puede tener muchas tareas. Columnas: usuario, categoria e identificador de tarea.

-- Controlador --

- Se crean las funcionalidades correspondientes a crear categoria, modificar categoria en una tarea, añadir una tarea a una categoria y listar tareas por categoria.

-- Test --

- Se prueban todas las funcionalidades en los modulos y en la aplicacion 
