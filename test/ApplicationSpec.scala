package test

import org.specs2.mutable._
import play.api.test._
import play.api.test.Helpers._
import org.specs2.runner._
import org.junit.runner._

// Modelos

import models._

// JSON

import play.api.mvc._
import play.api.libs.json.{Json, JsValue, JsArray}
import play.api.libs.functional.syntax._

//Date

import java.util.Date
import java.text.SimpleDateFormat
import org.specs2.matcher._

/**
 * Add your spec here.
 * You can mock out a whole application including requests, plugins etc.
 * For more information, consult the wiki.
 */
@RunWith(classOf[JUnitRunner])
class ApplicationSpec extends Specification {

  "Application" should {

    "send 404 on a bad request" in new WithApplication{
      route(FakeRequest(GET, "/boum")) must beNone
    }

    "render the index page" in new WithApplication{
      val home = route(FakeRequest(GET, "/")).get

      status(home) must equalTo(OK)
      contentType(home) must beSome.which(_ == "text/html")
     // contentAsString(home) must contain ("Your new application is ready.")
    }  


   "create category" in  new WithApplication {  

      val Some(result) = route(  
          FakeRequest(POST, "/juan.perez/category").withFormUrlEncodedBody(("label","unica")) 
          )
        status(result) must equalTo(201)         

     }

   "add existent category " in  new WithApplication {  

      Category.create("juan.perez","original")

      val Some(result) = route(  
          FakeRequest(POST, "/juan.perez/category").withFormUrlEncodedBody(("label","original")) 
          )
        status(result) must equalTo(409)         

     }

    "add task to category" in new WithApplication{ 

      Category.create("juan.perez","fast")

      val id = Task.create("task for app test4", "juan.perez")

      val Some(result) = route(  
          FakeRequest(POST, "/juan.perez/fast/"+id)  
          )
        status(result) must equalTo(201)  
     }

     "modify category of task" in new WithApplication{  

      val id = Task.create("task_for_app_test2","juan.perez")
      Category.create("juan.perez", "cartilla")
      Category.addTask("juan.perez", "cartilla", id)
     
        val Some(result) = route(  
          FakeRequest(POST, "/juan.perez/cartilla/"+id+"/modify").withFormUrlEncodedBody(("label","carterilla"))  
          )
        status(result) must equalTo(201)  
     }

  
     "list tasks for category" in new WithApplication{  

      Category.create("juan.perez", "prioridad")

      val taskId11 =      Task.create("task_for_test_list11","juan.perez")
      val taskId22 =      Task.create("task_for_test_list22","juan.perez")
      val taskId33 =      Task.create("task_for_test_list33","juan.perez")
      val taskId44 =      Task.create("task_for_test_list44","juan.perez")

      Category.addTask("juan.perez", "prioridad", taskId11)
      Category.addTask("juan.perez", "prioridad", taskId22)
      Category.addTask("juan.perez", "prioridad", taskId33)
      Category.addTask("juan.perez", "prioridad", taskId44)

      val Some(result) = route(  
        FakeRequest(GET, "/juan.perez/prioridad"))  

      status(result) must equalTo(OK)  
      contentType(result) must beSome.which(_ == "application/json")

         }
    
   
       }

      }
