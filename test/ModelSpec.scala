package test

import org.specs2.mutable._  
import play.api.test._  
import play.api.test.Helpers._
import org.specs2.runner._
import org.junit.runner._

import models._

@RunWith(classOf[JUnitRunner])
class ModelSpec extends Specification {

    def dateIs(date: java.util.Date, str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").format(date) == str  
    def strToDate(str: String) = new java.text.SimpleDateFormat("yyyy-MM-dd").parse(str)

    "Models" should {

      "create category" in {  
            running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

               
               Category.create("juan.perez", "prioridad")
               val exist = Category.exists("juan.perez", "prioridad")
           
               exist must equalTo(true)                  
               

            }
          }

      "add task to category" in {  
            running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

               Category.create("juan.perez", "prioridad")
               val taskId: Long = Task.create("task_for_test_add","juan.perez")
               val create: Boolean = Category.addTask("juan.perez", "prioridad", taskId)
               val existsTask = Category.existsTask("juan.perez", "prioridad", taskId)
               create must equalTo(true)
               existsTask must equalTo(true)  
            }
          }  

      "modify category of task" in {  
            running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

              
               Category.create("juan.perez", "prioridad")
               val taskId: Long = Task.create("task_for_test_modify","juan.perez")
               val addTask = Category.addTask("juan.perez", "prioridad", taskId)
              
               val modify = Category.modifyCategory("juan.perez", "prioridad", taskId, "prioritario")

               val ok = Category.existsTask("juan.perez", "prioritario", taskId)   
               val bad = Category.existsTask("juan.perez", "prioridad", taskId)           

               modify must equalTo(true)
               ok must equalTo(true)
               bad must equalTo(false)  

            }

          }   

      "list tasks for category" in {  
            running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

             Category.create("juan.perez", "prioridad")

           val taskId1 =  Task.create("task_for_test_list1","juan.perez")
            val taskId2 =      Task.create("task_for_test_list2","juan.perez")
            val taskId3 =      Task.create("task_for_test_list3","juan.perez")
            val taskId4 =      Task.create("task_for_test_list4","juan.perez")

               Category.addTask("juan.perez", "prioridad", taskId1)
               Category.addTask("juan.perez", "prioridad", taskId2)
               Category.addTask("juan.perez", "prioridad", taskId3)
               Category.addTask("juan.perez", "prioridad", taskId4)

               val list: List[Task] = Category.tasks("juan.perez", "prioridad")

               var count = 1

               val taskId1get = list.lift(0).get.label
               val taskId2get = list.lift(1).get.label
               val taskId3get = list.lift(2).get.label
               val taskId4get = list.lift(3).get.label

                taskId1get must equalTo("task_for_test_list1")
                taskId2get must equalTo("task_for_test_list2")
                taskId3get must equalTo("task_for_test_list3")
                taskId4get must equalTo("task_for_test_list4")                                
               

            }
          }

    "Crear una tarea" in {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      val size = Task.all().size
      Task.create("label1", "anonymous", None)
      Task.all().size - size must equalTo(1)
    }
  }

  "Obtener tarea por id OK" in {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      val id = Task.create("label2", "anonymous", None)
      val Some(task) = Task.findById(id)
      task.label must be equalTo("label2")
      task.taskOwner must be equalTo("anonymous")
    }
  }

  "Obtener tarea por id KO" in {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      val task = Task.findById(99999)
      task must be (None)
    }
  }


"Eliminar tarea por id OK" in {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {

      //creamos una tarea
      val id = Task.create("label3", "anonymous", None)

      //comprobamos que la tarea se ha creado correctamente
      val Some(task) = Task.findById(id)
      task.label must be equalTo("label3")
      task.taskOwner must be equalTo("anonymous")

      //eliminamos la tarea
      val result = Task.delete(id)
      result must beTrue

      //comprobamos que que la tarea no existe
      val task2 = Task.findById(id)
      task2 must be (None)
    }
  }  

"Eliminar tarea por id KO" in {
    running(FakeApplication(additionalConfiguration = inMemoryDatabase())) {
      val size = Task.all().size
      val result = Task.delete(9999)
      result must beFalse
      Task.all().size must equalTo(size)
    }
  }                                 

    }


}